<?php
/*
Plugin Name: Course Verification System
Plugin URI: 
Description: This short code Course Verification System 
Author: Agile Solutions PK
Version: 1.2
Author URI: http://agilesolutionspk.com
*/
if(!class_exists('agile_course_verification')){
	class agile_course_verification{
		
		function __construct(){
			register_activation_hook( __FILE__, array($this, 'install') );
			register_deactivation_hook( __FILE__, array($this, 'de_activate') );
			add_action('admin_menu', array(&$this,'admin_menu'));
			add_action('admin_enqueue_scripts', array(&$this, 'admin_enqueue_scripts') );
			add_action( 'wp_enqueue_scripts', array(&$this, 'wp_enqueue_scripts') );
			add_shortcode('verifycert',array(&$this, 'frontend'));
		}
		
		function frontend(){
			if(isset($_POST['submit'])){
				$name = $_POST['name'];
			}
			?>
			<form  method="post" action="">
					<div style = "float:left; clear:left;margin-top:1em" >
						<div style = "float:left; width:10em;" > Certificate Number</div>
						<div style = "float:left; width:25em;"><input required style = "width:20em;" type = "text" name = "name" value = "" /></div>
					</div>
					</div>
						<div style = "float:left; clear:left;  margin-top:1em">
						<div style = "float:right; width:25em;"><input  required type = "submit" name = "submit" value = "Submit" /></div>
					</div>
					</form>
		<?php
		}
		
		
		function wp_enqueue_scripts(){
			$deps = array('jquery','jquery-ui-core');
			wp_enqueue_script( 'jquery-ui-dialog', '', $deps);
			wp_enqueue_style( 'jquery_ui_style', plugins_url('css/jquery-ui.css', __FILE__) );
		}
		
		function admin_enqueue_scripts(){
			$this->wp_enqueue_scripts();
		
		}
		
		function install(){
			global $wpdb;	
			
			$sql="
			CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."student_record`(
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`name` VARCHAR( 255 ) NOT NULL ,
			`mail` VARCHAR( 255 ) NOT NULL ,
			`idcard` INT( 255 ) NOT NULL ,
			`course` VARCHAR( 255 ) NOT NULL ,
			`startdate` INT( 255 ) NOT NULL ,
			`enddate` INT( 255 ) NOT NULL ,
			 PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
			$wpdb->query($sql);
			$sql1="
			CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."certificate_table` (
				`id` INT( 11 ) NOT NULL AUTO_INCREMENT,
				`certificate_number` VARCHAR( 255 ) NOT NULL ,
				`student_form_id` INT( 11 ) NOT NULL,
				 PRIMARY KEY (`id`)
			) ENGINE = MYISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
			$wpdb->query($sql1);
		}		
		function insert_st_record($name,$email,$idcard,$course ,$startdate,$enddate){
			
			global $wpdb;
			$sql="Insert into {$wpdb->prefix}student_record (name,mail,idcard,course,startdate,enddate) values('{$name}','{$email}','{$idcard}','{$course}','{$startdate}','{$enddate}')";
			$wpdb->query($sql);
			
			$last_insert_id = $wpdb->insert_id; // get last inserted id
			return $last_insert_id;
		}
		
  		function de_activate(){
		
		
		}
		
		function student_entry_form(){
			
			if(isset($_POST['submit'])){
				$name = $_POST['name'];
				$email = $_POST['email'];
				$idcard =$_POST['idcard'];
				$course = $_POST['course'];
				$startdate = $_POST['startdate'];
				$enddate =$_POST['enddate'];
				$last_insert_id = $this->insert_st_record($name,$email,$idcard,$course ,$startdate,$enddate);
				$this->insert_certificate_no($last_insert_id);
			}
			
			?>
			<h2> STUDENT FORM</h2>
			<form id="frm_student" method="post" action="">
				<div style = "float:left; clear:left;margin-top:1em" >
					<div style = "float:left; width:10em;" > Name</div>
					<div style = "float:left; width:25em;"><input id="name" required style = "width:20em;" type = "text" name = "name" value = "<?php if($name) echo $name; ?>" /></div>
				</div>
				<div style = "float:left; clear:left;margin-top:1em" >
					<div style = "float:left; width:10em;" >E-Mail</div>
					<div style = "float:left; width:25em;"><input id="email"  style = "width:20em;" type = "text" name = "email" value = "<?php if($email) echo $email; ?>" /></div>
				</div>	
				
				<div style = "float:left; clear:left;margin-top:1em" >
					<div style = "float:left; width:10em;" >ID-Card</div>
					<div style = "float:left; width:25em;"><input required style = "width:20em;" type = "text" name = "idcard" value = "<?php if($idcard) echo $idcard; ?>" /></div>
				</div>
				<div style = "float:left; clear:left;margin-top:1em" >
					<div style = "float:left; width:10em;" >Course</div>
					<div  style = "float:left; width:25em;"><input required style = "width:20em;" type= "text" name = "course" value = "<?php if($course) echo $course; ?>" /></div>
				</div>
				<div style = "float:left; clear:left;margin-top:1em" >
					<div style = "float:left; width:10em;" >Start-date</div>
					<div style = "float:left; width:25em;"><input  required style = "width:20em;" type= "text" name = "startdate" value = "<?php if($startdate) echo $startdate; ?>" /></div>
				</div>
				<div style = "float:left; clear:left;margin-top:1em" >
					<div style = "float:left; width:10em;" >End-date</div>
					<div style = "float:left; width:25em;"><input required style = "width:20em;" type = "text" name = "enddate" value = "<?php if($enddate) echo $enddate; ?>" /></div>
				</div>
					<div style = "float:left; clear:left;  margin-top:1em">
					<div style = "float:right; width:25em;"><input id="submit" required type = "submit" name = "submit" value = "Submit" /></div>
				</div>
			</form>
			<div id="student_msg" title="Student Form" ></div>
			<script>
			
			jQuery("#frm_student").submit(function(){
				var txt = jQuery('#email').val();
				var re = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
				/* if (!re.test(txt)){
					//alert('Please enter a valid email address.');
					jQuery('#student_msg').html('Please enter a valid email address.');
					jQuery('#student_msg').dialog();
					return false;
				} */
				
			
				var n = jQuery("name").val();
				if(n.length < 3){
					jQuery('#student_msg').html('Name length is shorter than 3 characters.');
					jQuery('#student_msg').dialog();
					return false;
				}	
			
			});
			<?php $result = $this->get_student(); ?>
			</script>		
				<div style = "float:left; clear:left;margin-top:1em; background-color:gray;" >
					<div style = "float:left; width:10em;" > Roll No</div>
					<div style = "float:left; width:10em;" > Name</div>
					<div style = "float:left; width:15em;" >E-Mail</div>
					<div style = "float:left; width:10em;" >ID-Card</div>
					<div style = "float:left; width:10em;" >Course</div>
					<div style = "float:left; width:10em;" >Start-date</div>
					<div style = "float:left; width:10em;" >End-date</div>
				</div>
			<?php
			foreach($result as $r){
				$roll_no = $this->get_student_roll_no($r->id);
				?>
				<div style = "float:left; clear:left;" >
					<div style = "float:left; width:10em;" > &nbsp;<?php echo $roll_no; ?></div>
					<div style = "float:left; width:10em;" > <?php echo $r->name; ?></div>
					<div style = "float:left; width:15em;" ><?php echo $r->mail; ?></div>
					<div style = "float:left; width:10em;" ><?php echo $r->idcard; ?></div>
					<div style = "float:left; width:10em;" ><?php echo $r->course; ?></div>
					<div style = "float:left; width:10em;" ><?php echo $r->startdate; ?></div>
					<div style = "float:left; width:10em;" ><?php echo $r->enddate; ?></div>
				</div>
				<?php
			}
		}
	
	function show_form(){
	
	
	}
	
	
		function get_student(){
			
			global $wpdb;
			
			$sql="Select * from {$wpdb->prefix}student_record";
			$rs = $wpdb->get_results($sql);
			return $rs;
		}
		
		function get_student_roll_no($id){
			
			global $wpdb;
			
			$sql="Select certificate_number from {$wpdb->prefix}certificate_table where student_form_id = {$id}";
			$rs = $wpdb->get_var($sql);
			return $rs;
		}
		
		function admin_menu(){
			
			add_menu_page('Certification', 'Certification', 'manage_options', 'aspk_course_certification', array(&$this, 'Course_feilds') );
			add_submenu_page('aspk_course_certification' ,'Student Form', 'Student Form', 'manage_options', 'aspk_student_entry_form', array(&$this, 'student_entry_form') );
		}
				
		function Course_feilds(){
			
		}
		

  
  }//class bracket
  
  }//condition bracket
	
new agile_course_verification();
